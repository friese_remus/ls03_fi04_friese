package view.menue;

import controller.UserController;
import model.User;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDate;

public class CreateUser {
    private JTextField first_name;
    private JPanel mainPanel;
    private JTextField sur_name;
    private JTextField birthday;
    private JTextField street;
    private JTextField house_number;
    private JTextField postal_code;
    private JTextField city;
    private JTextField login_name;
    private JTextField password;
    private JTextField salary_expectations;
    private JTextField marital_status;
    private JTextField final_grade;
    private JButton submit;

    public CreateUser(UserController userController) {
        JFrame frame = new JFrame("Create User");
        frame.setContentPane(this.mainPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);

        submit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(validate()){
                    User user = new User(0,
                            first_name.getText(),
                            sur_name.getText(),
                            LocalDate.parse(birthday.getText()),
                            street.getText(),
                            house_number.getText(),
                            postal_code.getText(),
                            city.getText(),
                            login_name.getText(),
                            password.getText(),
                            Integer.parseInt(salary_expectations.getText()),
                            marital_status.getText(),
                            Double.parseDouble(final_grade.getText()));
                    userController.createUser(user);
                    frame.dispose();
                }
            }
        });
    }
    private boolean validate(){
        String[] userData = {
                first_name.getText(),
                sur_name.getText(),
                birthday.getText(),
                street.getText(),
                house_number.getText(),
                postal_code.getText(),
                city.getText(),
                login_name.getText(),
                password.getText(),
                salary_expectations.getText(),
                marital_status.getText(),
                final_grade.getText()
        };
        for(String userDataEntry : userData) {
            if (userDataEntry == null || userDataEntry.isEmpty()) {
                return false;
            }
        }
        return true;
    }
}
