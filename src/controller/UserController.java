package controller;

import model.User;
import model.persistance.IUserPersistance;
import model.persistance.IPersistance;

/**
 * controller for users
 * @author Clara Zufall
 * TODO implement this class
 */
public class UserController {

    private IUserPersistance userPersistance;

    public UserController(IPersistance persistence) {
        this.userPersistance = persistence.getUserPersistance();
    }

    public int createUser(User user) {
        if(readUser(user.getLoginname(), "") == null){
            return userPersistance.createUser(user);
        }
        return 0;
    }

    /**
     * liest einen User aus der Persistenzschicht und gibt das Userobjekt zur�ck
     * @param username eindeutige Loginname
     * @param passwort das richtige Passwort
     * @return Userobjekt, null wenn der User nicht existiert
     */
    public User readUser(String username, String passwort) {
        return userPersistance.readUser(username);
    }

    public void changeUser(User user) {

    }

    public void deleteUser(User user) {
        userPersistance.deleteUser(user.getP_user_id());
    }

    public boolean checkPassword(String username, String passwort) {
        User user = readUser(username, passwort);
        return user.getPassword().equals(passwort);
    }
}
